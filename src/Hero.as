package
{
	import flash.display.Sprite;
	//import com.demonsters.debugger.MonsterDebugger;
	import utils.sounds;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Hero extends HeroDesign
	{
		
		private var _size:Number = 0.2;
		private var _dirAngle:Number = 0;
		private var _speed:Number = 2;
		
		private var i:Number = 0;
		
		public function Hero()
		{
			scaleX = _size;
			scaleY = _size;
		}
		
		public function doo():void
		{
			
			rotation = Math.atan2(Main.mouse_y - y, Main.mouse_x - x) * 180 / Math.PI;
			if (Main.mouseLeftDown)
			{
				
				if (!(x >= Main.mouse_x - 10 && x <= Main.mouse_x + 10 && y >= Main.mouse_y - 10 && y <= Main.mouse_y + 10))
				{
					_dirAngle = rotation / 180 * Math.PI;
					
					x = x + _speed * Math.cos(_dirAngle);
					y = y + _speed * Math.sin(_dirAngle);
				}
				
			}
		
		}
		
		public function collision(arr:Array):void
		{
			
			for (i = 0; i <= Gameplay.ENEMY_COUNT; i++)
			{
				
				if (visible)
				{
					if (arr[i] != null)
					{
						
						if (hitTestObject(arr[i]) && arr[i].visible == true)
						{
							
							if (_size >= arr[i].size)
							{
								arr[i].dead();
								arr[i].visible = false;
								size = size + 0.1;
								sounds.PlaySnd("sndEat");
							}
							else
							{
								arr[i].size = arr[i].size + 0.1;
								dead();
								sounds.PlaySnd("sndEat");
							}
						}
					}
					
				}
				
			}
		
		}
		
		public function dead():void
		{
			
			visible = false;
		}
		
		public function get size():Number
		{
			return _size;
		}
		
		public function set size(value:Number):void
		{
			_size = value;
			scaleX = _size;
			scaleY = _size;
			if (_speed > 1)
				_speed = _speed - 0.1;
		}
		
		public function clear():void
		{
		
		}
	
	}

}