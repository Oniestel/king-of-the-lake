package
{
	
	import utils.sounds;
	import mochi.as3.*;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class GameMenu extends GameMenuDesign
	{
		
		private var _btnSubmitClk:Boolean = false;
		private var _btnSubmitSound:Boolean = false;
		
		private var _btnAgainClk:Boolean = false;
		private var _btnAgainSound:Boolean = false;
		
		public var score:Number = 0;
		
		public function GameMenu()
		{
			
			btnSubmit.alpha = 0.7;
			btnAgain.alpha = 0.7;
		
		}
		
		public function doo():void
		{
			
			if (btnSubmit.hitTestPoint(Main.mouse_x, Main.mouse_y, true) && btnSubmit.visible)
			{
				
				btnSubmit.alpha = 1;
				
				if (_btnSubmitSound == false)
				{
					sounds.PlaySnd("sndButton");
					_btnSubmitSound = true;
				}
				
				if (Main.mouseLeftDown)
				{
					_btnSubmitClk = true;
					//Send score to mochi
				}
				
			}
			else
			{
				_btnSubmitSound = false;
				btnSubmit.alpha = 0.7;
			}
			
			if (btnAgain.hitTestPoint(Main.mouse_x, Main.mouse_y, true))
			{
				
				btnAgain.alpha = 1;
				
				if (_btnAgainSound == false)
				{
					sounds.PlaySnd("sndButton");
					_btnAgainSound = true;
				}
				
				if (Main.mouseLeftDown)
				{
					_btnAgainClk = true;
				}
				
			}
			else
			{
				_btnAgainSound = false;
				btnAgain.alpha = 0.7;
			}
		
		}
		
		public function get btnAgainClk():Boolean
		{
			return _btnAgainClk;
		}
		
		public function clear():void
		{
		
		}
	
	}

}