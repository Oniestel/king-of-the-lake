package
{
	import flash.display.Sprite;
	import com.greensock.*;
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	//import com.demonsters.debugger.MonsterDebugger;
	import utils.musicEnv;
	import utils.sounds;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Gameplay extends Sprite
	{
		
		public static var ENEMY_COUNT:Number = 30;
		
		public var reloadGame:Boolean = false;		
		
		private var _hero:Hero;
		private var _enemyArr:Array;
		private var i:Number = 0;
		private var _backBubbles:Bubbles = new Bubbles();
		private var _gui:GuiDesign = new GuiDesign();
		private var _sec:Number = 900;
		private var _time:Number = 0;
		private var _endMenu:GameMenu = new GameMenu();
		private var _anyEnemyIsAlive:Boolean = true;
		private var _tempEnemyVisibleStateCount:Number = 0;
		private var _gameEnd:Boolean;
		private var _endState:Number = 0;
		private var _help:helpDesign = new helpDesign();
		
		public function Gameplay()
		{
			_hero = new Hero();
			_enemyArr = new Array();
			
			addChild(_backBubbles);
			
			for (i = 0; i <= ENEMY_COUNT; i++)
			{
				
				_enemyArr[i] = new Enemy();
				addChild(_enemyArr[i]);
			}
			
			_hero.x = 0;
			_hero.y = 0;
			
			addChild(_hero);
			addChild(_gui);
			
			_endMenu.x = 176;
			_endMenu.y = -500;
			
			addChild(_endMenu);
			
			musicEnv.fadeTo("mus1");
			
			_gui.x = 10;
			_gui.y = 4;
			
			_help.x = 28;
			_help.y = 75;
			
			addChild(_help);
			
			TweenMax.to(_help, 5, {alpha: 0});
		
		}
		
		private function timerComplete(e:TimerEvent):void
		{
			
			for (i = 1; i <= 10; i++)
			{
				
				_enemyArr[i + ENEMY_COUNT] = new Enemy();
				addChild(_enemyArr[i + ENEMY_COUNT]);
				
			}
			
			ENEMY_COUNT += 10;
			_sec = 30;
		
		}
		
		public function doo():void
		{
			
			if (!_gameEnd)
			{
				_sec--;
				
				_time++;
				
				if (_sec <= 0)
				{
					
					_sec = 900;
					
					for (i = 1; i <= 10; i++)
					{
						
						_enemyArr[i + ENEMY_COUNT] = new Enemy();
						addChild(_enemyArr[i + ENEMY_COUNT]);
						
					}
					
					ENEMY_COUNT += 10;
					
				}
				
				_gui.guiLeft.text = "More rivals in " + Math.round(_sec / 30) + " sec";
				_gui.guiRight.text = Math.round(_time / 30) + " sec";
				
				_hero.doo();
				_hero.collision(_enemyArr);
				
				_tempEnemyVisibleStateCount = 0;
				
				for (i = 0; i <= ENEMY_COUNT; i++)
				{
					
					_enemyArr[i].doo();
					
					_enemyArr[i].collision(_enemyArr, i);
					
					if (_enemyArr[i].visible == true)
						_tempEnemyVisibleStateCount++;
					
				}
				
				if (_tempEnemyVisibleStateCount == 0)
				{
					
					_gameEnd = true;
					TweenMax.to(_endMenu, 2, {y: 222});
					_endState = 1;
					_endMenu.fldTitle.text = "You win!";
					_endMenu.fldScore.text = Math.round(_time / 30) + " sec";
					_endMenu.score = Math.round(_time / 30);
					
				}
				
				if (_hero.visible == false)
				{
					
					_gameEnd = true;
					TweenMax.to(_endMenu, 2, {y: 222});
					_endState = 2;
					_endMenu.fldTitle.text = "You lose!";
					_endMenu.fldScore.visible = false;
					_endMenu.btnSubmit.visible = false;
					_endMenu.btnAgain.y = _endMenu.btnAgain.y - 80;
				}
				
			}
			else
			{
				
				_endMenu.doo();
				
			}
			
			reloadGame = _endMenu.btnAgainClk;
		
		}
		
		public function clear():void
		{
			
			removeChild(_help);
			removeChild(_hero);
			removeChild(_gui)
			removeChild(_endMenu);
			
			for (i = 0; i <= ENEMY_COUNT; i++)
			{
				removeChild(_enemyArr[i]);
				_enemyArr[i].clear();
				_enemyArr[i] = null;
			}
			
			_backBubbles.clear();
			_hero.clear();
			_endMenu.clear();
			
			_backBubbles = null;
			_hero = null;
			_help = null;
			_gui = null;
		}
	
	}

}