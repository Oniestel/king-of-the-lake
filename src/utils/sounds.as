﻿// Управление пространственными звуками в игре
package utils {
	import flash.media.SoundTransform;
	import flash.display.*;
	import flash.utils.*;
	import flash.geom.*;

	dynamic public class sounds extends Object 
	{
		static private var snd:Object;	// хэш со звуками
		static private var stageRadius:Number; // примерный радиус видимой области
		static public var vol:Number=0.9; // громкость
		static public var sndEnable:Boolean=true; // включён ли звук
		
		//Координаты слушателя
		static public var cenX:Number=0;
		static public var cenY:Number=0;
		
		//внутренние переменные
		static private var v:ageVector= new ageVector();
		static private var p:Point;
		
		//Инициализация класса. Передаём в функцию размеры флешки
		static public function init(stageWidth:Number, stageHeight:Number) :void
		{
			// создадим хеш звуков
			snd = new Object();
			
			//Здесь втыкаем свои звуки
			snd['sndEat'] = new sndEat();
			snd['sndButton'] = new sndButton();
			snd['sndEnv'] = new sndEnv();


			
			// вычислим примерный радиус видимой области экрана
			stageRadius = (stageWidth)/1.4;			
			
			//Центр по умолчанию в центре экрана. Можно привязать к какому-либо объекту
			cenX=stageWidth*0.5;
			cenY=stageHeight*0.5;

		}
		

		// Воспроизводит звук с учетом пространства
		// snd_name - имя звука
		// dv - множитель для громкости		
		// obj - эмиттер звука
		static public function PlaySnd(snd_name:String,dv:Number=1.0,obj:*=null):void 
		{
			var t:SoundTransform;
			
			//Если звук отключён - выходим
			if(!sndEnable || vol<0.001) return;
			
			//Без объёмного звука
			if(obj==null)
			{
				
				t = new SoundTransform();
				t.volume = vol;
				snd[snd_name].play(0, 0, t);

			}
			else
			{
				
				t= new SoundTransform();
				
				//Все расчёты ведём в глобальной системе координат
				p=obj.parent.localToGlobal(new Point(obj.x,obj.y));
				
				v.x=p.x-cenX;
				v.y=p.y-cenY;
				
				// Ставим громкость в зависимости от расстояния
				t.volume = ageMath.trimToRange(vol * ageMath.RemapVal(v.len(),0,dv * stageRadius,1,0),0,1);

				// стерео в зависимости от положения источника звука
				t.pan = ageMath.trimToRange(ageMath.RemapVal(v.x,-stageRadius/2,stageRadius/2,-1,1),-1,1);

				// воспроизводим
				snd[snd_name].play(0, 0, t);
			}
		}
		
	}
}
