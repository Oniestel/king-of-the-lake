﻿// ageVector
package utils 
{
	import utils.*; 
	public class ageVector 
	{
		public var x:Number;
		public var y:Number;

		public function ageVector(setX:Number=0, setY:Number=0)
		{
		   x = setX;
		   y = setY;
		}
		
		//Копирует вектор
		public function copy( v:ageVector ):void 
		{
			x = v.x;
			y = v.y;
		}
		
		//Устанавливает компоненты вектора
		public function set( setX:Number, setY:Number ):void 
		{
			x = setX;
			y = setY;
		}
		
		//Прибавляет вектор v
		public function add( v:ageVector ):void 
		{
			x += v.x;
			y += v.y;
		}
		
		//Вычитает вектор v
		public function sub( v:ageVector ):void 
		{
			x -= v.x;
			y -= v.y;
		}
		
		//Умножает вектор на число i
		public function mulScalar( i:Number ):void 
		{
			x *= i;
			y *= i;
		}
		
		//Равны ли вектора c заданной погрешностью aprox
		public function isEqual( v:ageVector , aprox:Number=0.00001):Boolean 
		{
			return (ageMath.aproxEqual(x,v.x,aprox) && ageMath.aproxEqual(y,v.y,aprox));
		}
		
		//Длина вектора
		public function len():Number 
		{
			return Math.sqrt( x*x + y*y );
		}
		
		//Длина вектора в квадрате
		public function len2():Number 
		{
			return x*x + y*y;
		}
		
		//Ограничить длину вектора максимальной длинной
		public function trimLen(maxLen:Number):void 
		{
			if(len2()>maxLen*maxLen)
			{
				normThis();
				mulScalar(maxLen);
			}
		}
		//Установить длину вектора
		public function setLen(maxLen:Number):void 
		{
			normThis();
			mulScalar(maxLen);
		}		
		
		//получить проекцию вектора на вектор v
		public function ProjOnVec(v:ageVector):ageVector 
		{
			var res:ageVector = v.norm();
			res.mulScalar(ProjOnScalar(v));
			return res;
		}
		
		// скалярная проекция вектора на вектор v
		public function ProjOnScalar(v:ageVector):Number 
		{
			return dot(v)/v.len();
		}		
		
		// скалярное произведение векторов
		public function dot(v:ageVector):Number 
		{
			return x*v.x + y*v.y;
		}

		// нормализация вектора (получить единичный вектор)
		public function norm():ageVector 
		{
			var l:Number = len();
			var res:ageVector = new ageVector(x,y);
			if (l) 
			{
				res.x /= l;
				res.y /= l;
			}
			return res;
		}
		
		// нормализация этого вектора 
		public function normThis():void 
		{
			var l:Number = len();
			if (l) 
			{
				x /= l;
				y /= l;
			}
		}
		
		// Lerp с другим вектором. Результат записывается в этот же вектор
		public function lerpThis(v:ageVector,s:Number):void 
		{
			var tmpV:ageVector = new ageVector();
			tmpV.copy(v);
			tmpV.sub(this);
			tmpV.mulScalar(s);
			add(tmpV);
		}
		
		//Поворот вектора на угл в радианах
		public function rot(ang:Number):void
		{
			var dx:Number=x;
			x=dx*Math.cos(ang)-y*Math.sin(ang);
			y=y*Math.cos(ang)+dx*Math.sin(ang);
		}
		
		//Поворот вектора на угл в градусах
		public function rotDeg(ang:Number):void
		{
			var dx:Number=x;
			var angR:Number=ang*Math.PI/180;
			x=dx*Math.cos(angR)-y*Math.sin(angR);
			y=y*Math.cos(angR)+dx*Math.sin(angR);
		}
		
		// получить угол в градусах
		public function getAngleDeg():Number 
		{
			return Math.atan2(y, x)/Math.PI*180;
		}
		
		// получить угол в радианах
		public function getAngle():Number 
		{
			return Math.atan2(y, x);
		}
		
		// получить угол в градусах с нормированным переходом для rotation мовиклипа 
		public function getAngleDegNorm():Number 
		{
			var angle:Number=Math.atan2(y, x)/Math.PI*180;
			
			// проблема с переходом -180...180 
			if (angle>180) angle-=360; 
			else 
			if (angle<-180) angle+=360; 
			
			return angle;
		}
		
		// получить угол в радианах  с нормированным переходом для rotation мовиклипа 
		public function getAngleNorm():Number 
		{
			var angle:Number=Math.atan2(y, x);
			
			// проблема с переходом -180...180 
			if (angle>Math.PI) angle-=Math.PI*2; 
			else 
			if (angle<-Math.PI) angle+=Math.PI*2; 
			
			return angle;			
		}
	}
}

