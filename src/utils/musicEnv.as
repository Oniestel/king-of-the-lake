﻿// Управление пространственными звуками в игре
package utils {
	import flash.media.SoundTransform;
	import flash.display.*;
	import flash.geom.*;
	import flash.utils.*;
	import flash.events.*;
	import flash.media.*;
	dynamic public class musicEnv extends Object 
	{
		static public var mus:Object;	// хэш с музыкой
		static public var vol:Number=0.9; // громкость
		static public var volFadeSpeed:Number=0.01; //Скорость фейдинга
		static public var musEnable:Boolean=true; // включёна ли музыка
		
		static public var curMusName:String=""; // Имя текущего трека
		
		
		//внутренние переменные
		static private var curCh:SoundChannel; //Текущий канал
		static private var offCh:SoundChannel; //Затухающий канал
		static private var curVol:Number=0;
		static private var curPos:Number=0;
		static private var offVol:Number=0;
		
		static private var isplayed:Boolean=false;
		
		static private var timerVolFader:Timer= new Timer(10);
		
		static private var t1:SoundTransform = new SoundTransform();
		static private var t2:SoundTransform = new SoundTransform();
		
		static public function init():void
		{
			timerVolFader.stop();
			
			// создадим хеш с музыкой
			mus = new Object();
			
			//Здесь втыкаем свои муз треки
			mus['mus1'] = new music1();
			mus['mus2'] = new sndEnv();			

		}
		
		//Плавная смена трека на musname
		static public function fadeTo(musname:String):void 
		{
			curMusName=musname;
			
			
			//Если музыка отключена - выходим
			if(!musEnable) return;
			
			isplayed=true;
			
			var t:SoundTransform;
			t = new SoundTransform();
			t.volume = 0;
			
			if(offCh) offCh.stop();
			offCh=curCh;
			
			curCh=mus[musname].play(0, int.MAX_VALUE, t);
					
			curVol=0;
			offVol=vol;
			
			if(!timerVolFader.running)
			{
				timerVolFader.addEventListener(TimerEvent.TIMER, timerVolFader_Timer, false, 0, true);
				timerVolFader.start();
			}
			
		}
		
		//Остановить или запустить воспроизведение музыки
		static public function played(flg:Boolean):void 
		{
			if(flg==isplayed) return;
			
			if(flg)
			{
				if(curMusName!="") fadeTo(curMusName);
			}
			else
			{
				isplayed=false;
				
				if(curCh) 
				{
					curCh.stop();
					curPos=curCh.position;
				}
				if(offCh) offCh.stop();
				offVol=0;
				curVol=vol;
				
				if(timerVolFader.running)
				{
					timerVolFader.stop();
					timerVolFader.removeEventListener(TimerEvent.TIMER, timerVolFader_Timer);
				}
			}
		}
		
		//Плавный фейдинг музыки
		static public function timerVolFader_Timer(event:TimerEvent):void 
		{
			
			
			if(offCh!=null)
			{
				if(offVol>0)
					offVol -= volFadeSpeed;
				else
					offVol=0;
	
				t1.volume = offVol;
				offCh.soundTransform=t1;
			}
			
			if(curCh!=null && (offVol<vol/2 || offCh==null))
			{
				if(curVol<vol)
					curVol += volFadeSpeed;
				else
					curVol=vol;
	
				t2.volume = curVol;
				curCh.soundTransform=t2;
			}
				
			
			//Если достигли порогового значения громкости
			if(curVol>vol)
			{
				if(offCh!=null) offCh.stop();
				timerVolFader.stop();
				timerVolFader.removeEventListener(TimerEvent.TIMER, timerVolFader_Timer);
			}
		}
	}
}
