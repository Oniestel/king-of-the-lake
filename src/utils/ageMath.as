﻿package utils { 
     import utils.*; 
	 import flash.geom.*;
	 import flash.display.*;

     dynamic public class ageMath 
	 {

			public function ageMath() 
			{ 
				
			} 
			
			//Минимальное из 3
			static public function min3(val1:Number,val2:Number,val3:Number):Number
			{
				return Math.min(val1,Math.min(val2,val3));
			}
			//Максимальное из 3
			static public function max3(val1:Number,val2:Number,val3:Number):Number
			{
				return Math.max(val1,Math.max(val2,val3));
			}
			
			//Находится ли Num в диапазоне lRange..uRange
		    static public function inRange(Num:Number,lRange:Number,uRange:Number):Boolean
			{
			   return ((Num > lRange) && (Num < uRange));
			}
			
			//Обрезать до диапазона lRange..uRange
		    static public function trimToRange(Num:Number,lRange:Number,uRange:Number):Number
			{
			   return ((Num>uRange)?uRange:((Num<lRange)?lRange:Num));
			}
			
			//получить ближайшие число к arg - Val1 или Val2
		    static public function getClosest(arg:Number,Val1:Number,Val2:Number):Number
			{
				if(Math.abs(arg - Val1)< Math.abs(arg - Val2)) return Val1;
				return Val2;
			}
			
		    //Получить случайное число из диапазона clow..chigh
			static public function Random(clow:Number, chigh:Number):Number
			{
			  return Math.round(Math.random() * (chigh - clow)) + clow;
			}
			
			//Получить целое случайное число из диапазона clow..chigh
			static public function RandomInt(clow:int, chigh:int):int
			{
			  return Math.round(Random(clow,chigh));
			}
			
			//Равны ли числа Num и aNum с заданной погрешностью aprox
			static public function aproxEqual(Num:Number, aNum:Number, aprox:Number=0.00001):Boolean
			{
			  return (Math.abs(aNum-Num)<=aprox);
			}
			
			//Перевести число val из диапазона A..B в диапазон C..D
			static public function RemapVal(val:Number,A:Number,B:Number,C:Number,D:Number):Number
			{
				return C + (D - C) * (val - A) / (B - A);
			}
			
			//Получить число из диапазона cx..cy, зависящей от коэфициента s.
			//если s=0.0 то возвращается cx.
			//если s=1.0 то возвращается cy. 
			static public function Lerp(cx:Number,cy:Number,s:Number):Number
			{
				return cx + s*(cy - cx);
			}
			
			//расстояние между точками
			static public function Distance(x1:Number,y1:Number,x2:Number,y2:Number):Number
			{
				return Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
			}
			
			//расстояние между векторами
			static public function DistanceVec(v1:ageVector,v2:ageVector):Number
			{
				return Distance(v1.x,v1.y,v2.x,v2.y);
			}
			
			//Пересекаются ли две линии.
			static public function IsLinesCross(x11:Number,y11:Number,x12:Number,y12:Number, 
							    x21:Number,y21:Number,x22:Number,y22:Number):Boolean
			{
			
				// решаем систему методом Крамера
				var d:Number = (x12 - x11)*(y21 - y22) - (x21 - x22)*(y12 - y11);
				
				if (d == 0) return false;//Отрезки либо параллельны, либо полностью/частично совпадают
				
				var d1:Number = (x21 - x11)*(y21 - y22) - (x21 - x22)*(y21 - y11);
				var d2:Number = (x12 - x11)*(y21 - y11) - (x21 - x11)*(y12 - y11);
				
				var t1:Number = d1 / d;
				var t2:Number = d2 / d;
				
				if (t1 >= 0 && t1 <= 1 && t2 >= 0 && t2 <= 1) 
					return true; //пересекаются
				else
					return false; //не пересекаются
			}
			
			//Получить точку пересечения двух линий
			static public function getLinesCross(x11:Number,y11:Number,x12:Number,y12:Number, 
							    x21:Number,y21:Number,x22:Number,y22:Number):ageVector
			{
			
				// решаем систему методом Крамера
				var d:Number = (x12 - x11)*(y21 - y22) - (x21 - x22)*(y12 - y11);
				
				if (d == 0) return null;//Отрезки либо параллельны, либо полностью/частично совпадают
				
				var d1:Number = (x21 - x11)*(y21 - y22) - (x21 - x22)*(y21 - y11);
				var d2:Number = (x12 - x11)*(y21 - y11) - (x21 - x11)*(y12 - y11);
				
				var t1:Number = d1 / d;
				var t2:Number = d2 / d;
				
				if (t1 >= 0 && t1 <= 1 && t2 >= 0 && t2 <= 1) 
				{
					//пересекаются
					var vTmp:ageVector=new ageVector();
					vTmp.set(t1*x12 + (1-t1)*x11,t1*y12 + (1-t1)*y11);
					return vTmp; 
				}
				else
				{
					//не пересекаются
					return null; 
				}
			}
			//Получить точку пересечения двух линий
			static public function getLineCrossRect(
								x1:Number,y1:Number,x2:Number,y2:Number,  //Координаты линии
							    xmin:Number,ymin:Number,xmax:Number,ymax:Number   //Координаты прямоугольника
								):ageVector
			{		
				var dx:Number, dy:Number, u1:Number=0.0, u2:Number=1.0;
				var p0:Number,p1:Number,p2:Number,p3:Number,
					q0:Number,q1:Number,q2:Number,q3:Number,
					r0:Number,r1:Number,r2:Number,r3:Number;
				
	
				
				dx = x2 - x1; 
				dy = y2 - y1; 
				

				
				p0 = -dx; 
				p1 = dx; 
				p2 = -dy; 
				p3 = dy; 
				q0 = x1 - xmin; 
				q1 = xmax - x1; 
				q2 = y1 - ymin; 
				q3 = ymax - y1; 
				
				if( (p0 == 0 && q0 < 0) ||
					(p1 == 0 && q1 < 0) ||
					(p2 == 0 && q2 < 0) ||
					(p3 == 0 && q3 < 0)) 
				{ 
					//trace("THE LINE IS COMPLETELY OUTSIDE THE CLIPPING WINDOW........."); 
					return null;
				} 
				

				if(p0 < 0) { r0 = q0 / p0; u1 = Math.max(u1, r0); } 
				if(p1 < 0) { r1 = q1 / p1; u1 = Math.max(u1, r1); } 
				if(p2 < 0) { r2 = q2 / p2; u1 = Math.max(u1, r2); } 
				if(p3 < 0) { r3 = q3 / p3; u1 = Math.max(u1, r3); } 
				
				if(p0 > 0) { r0 = q0 / p0; u2 = Math.min(u2, r0); } 
				if(p1 > 0) { r1 = q1 / p1; u2 = Math.min(u2, r1); } 
				if(p2 > 0) { r2 = q2 / p2; u2 = Math.min(u2, r2); } 
				if(p3 > 0) { r3 = q3 / p3; u2 = Math.min(u2, r3); } 
				
				
				if(u1 > u2) 
				{ 
					//trace("THE LINE IS COMPLETELY OUTSIDE THE CLIPPING WINDOW........."); 
					return null;
				} 
				
				if(u1 == 0.0 && u2 == 1.0) 
				{ 
					//trace("THE LINE IS COMPLETELY INSIDE THE CLIPPING WINDOW........."); 
					return null;
				} 
				
				
				
				if(u1 < u2 && u1 != 0.0 && u2 != 1.0) 
				{ 
					//x2 = x1 + (dx * u2); 
					//y2 = y1 + (dy * u2); 
					//x1 = x1 + (dx * u1); 
					//y1 = y1 + (dy * u1); 
				
					//printf("THE REQUIRED CLIPPED LINE IS:");
					var newX1:Number= x1 + (dx * u1); 
					var newY1:Number= y1 + (dy * u1); 
					var newX2:Number= x1 + (dx * u2); 
					var newY2:Number= y1 + (dy * u2); 
					
					var vTmp:ageVector=new ageVector();
					if(Distance(newX1,newY1,x1,y1)<Distance(newX2,newY2,x1,y1))
					{
					
						vTmp.set(newX1,newY1);
						return vTmp; 
					}
					else
					{
						vTmp.set(newX2,newY2);
						return vTmp; 
					}
				}
				//trace("WTF?");
				return null;
			}
			
     } 
}