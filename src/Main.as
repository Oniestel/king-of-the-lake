package
{
	import flash.display.NativeMenu;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import utils.musicEnv;
	import utils.sounds;
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	//import com.demonsters.debugger.MonsterDebugger;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	[Frame(factoryClass="Preloader")]
	
	public class Main extends Sprite
	{
		
		public static var mouse_x:Number = 0;
		public static var mouse_y:Number = 0;
		public static var mouseLeftDown:Boolean = false;
		public static var pause:Boolean = false;
		public static var mouseState:Number = 0;
		
		private var _state:Number = 0;
		private var _game:Gameplay;
		private var _menu:MainMenu;
		
		public function Main():void
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			TweenPlugin.activate([FrameLabelPlugin, ColorTransformPlugin]);
			//MonsterDebugger.initialize(this);
			
			sounds.init(640, 640);
			musicEnv.init();
			
			musicEnv.fadeTo("mus2");
			
			_menu = new MainMenu();
			addChild(_menu);
			_menu.x = 135;
			_menu.y = 196;
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveEvent);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownEvent);
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpEvent);
			this.addEventListener(Event.ENTER_FRAME, gameProcess);
			this.addEventListener(Event.DEACTIVATE, mouseOut);
			this.addEventListener(Event.ACTIVATE, mouseIn);
		}
		
		private function mouseOut(e:Event):void
		{
			
			if (_state == 1)
			{
				pause = true;
					//pauseMenu.activate();
			}
		
		}
		
		private function mouseIn(e:Event):void
		{
			
			if (_state == 1)
			{
				pause = false;
					//pauseMenu.deactivate();
			}
		
		}
		
		private function mouseMoveEvent(e:MouseEvent):void
		{
			
			mouse_x = e.stageX;
			mouse_y = e.stageY;
		
		}
		
		private function mouseDownEvent(e:MouseEvent):void
		{
			
			mouseLeftDown = true;
		
		}
		
		private function mouseUpEvent(e:MouseEvent):void
		{
			
			mouseLeftDown = false;
		
		}
		
		private function gameProcess(e:Event):void
		{
			
			if (_state == 1)
			{
				_game.doo();
				
				if (_game.reloadGame == true)
				{
					_game.reloadGame = false;
					TweenMax.to(this, 1, {colorMatrixFilter: {colorize: 0x000000}});
					TweenMax.delayedCall(2, reloadGame);
					_state = 0;
				}
			}
			
			if (_state == 0)
			{
				_menu.doo();
				if (_menu.btnPlayClk)
				{
					_menu.btnPlayClk = false;
					TweenMax.to(this, 1, {colorMatrixFilter: {colorize: 0x000000}});
					TweenMax.delayedCall(2, changeScreen);
				}
			}
		
		}
		
		private function changeScreen():void
		{
			_state = 1;
			removeChildAt(0);
			_menu.clear();
			_game = new Gameplay();
			addChildAt(_game, 0);
			TweenMax.to(this, 0.5, {colorMatrixFilter: {}});
		
		}
		
		private function reloadGame():void
		{
			
			_state = 1;
			_game.clear();
			removeChildAt(0);
			_game = new Gameplay();
			addChildAt(_game, 0);
			TweenMax.to(this, 1, {colorMatrixFilter: {}});
		
		}
	
	}

}