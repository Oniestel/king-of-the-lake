package
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import org.flintparticles.common.counters.*;
	import org.flintparticles.common.displayObjects.RadialDot;
	import org.flintparticles.common.initializers.*;
	import org.flintparticles.twoD.actions.*;
	import org.flintparticles.twoD.emitters.Emitter2D;
	import org.flintparticles.twoD.initializers.*;
	import org.flintparticles.twoD.renderers.*;
	import org.flintparticles.twoD.zones.*;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Bubbles extends Sprite
	{
		
		public var pos_x:Number = 0;
		public var pos_y:Number = 0;
		
		public var emitter:Emitter2D = new Emitter2D();
		public var renderer:DisplayObjectRenderer = new DisplayObjectRenderer();
		
		public function Bubbles()
		{
			
			addChild(renderer);
			renderer.addEmitter(emitter);
			
			emitter.counter = new Steady(3);
			
			emitter.addInitializer(new ImageClass(RadialDot, 1.2));
			emitter.addInitializer(new Position(new LineZone(new Point(640, 0), new Point(640, 640))));
			emitter.addInitializer(new Velocity(new PointZone(new Point(-50, 0))));
			
			emitter.addAction(new Move());
			emitter.addAction(new RandomDrift(50, 50));
			emitter.addAction(new DeathZone(new RectangleZone(-10, -10, 650, 650), true));
			
			emitter.start();
		
		}
		
		public function clear():void
		{
			
			removeChild(renderer);
			emitter = null;
			renderer = null;

		
		}
	
	}

}