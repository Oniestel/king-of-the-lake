package
{
	
	import flash.display.Sprite;
	//import com.demonsters.debugger.MonsterDebugger;
	import utils.sounds;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Enemy extends EnemyDesign
	{
		
		public static const ENEMY_STOP:Number = 0;
		public static const ENEMY_WAIT:Number = 1;
		public static const ENEMY_GO:Number = 2;
		
		private var _size:Number = 0.1;
		private var _targetX:Number = 0;
		private var _targetY:Number = 0;
		private var _state:Number = 2;
		private var _speed:Number = 2;
		private var _dirAngle:Number = 0;
		private var i:Number = 0;
		
		public function Enemy()
		{
			x = Math.random() * (640 - width);
			y = Math.random() * (640 - height);
			scaleX = 0.1;
			scaleY = 0.1;
			
			_targetX = Math.random() * (640 - width);
			_targetY = Math.random() * (640 - height);
			
			rotation = Math.atan2(targetY - y, targetX - x) * 180 / Math.PI;
		}
		
		public function get targetX():Number
		{
			return _targetX;
		}
		
		public function set targetX(value:Number):void
		{
			_targetX = value;
		}
		
		public function get targetY():Number
		{
			return _targetY;
		}
		
		public function set targetY(value:Number):void
		{
			_targetY = value;
		}
		
		public function get state():Number
		{
			return _state;
		}
		
		public function set state(value:Number):void
		{
			_state = value;
		}
		
		public function get size():Number
		{
			return _size;
		}
		
		public function set size(value:Number):void
		{
			_size = value;
			scaleX = _size;
			scaleY = _size;
			if (_speed > 1)
				_speed = _speed - 0.1;
		}
		
		public function doo():void
		{
			
			if (x >= _targetX - 20 && x <= _targetX + 20 && y >= _targetY - 20 && y <= _targetY + 20)
			{
				
				_targetX = Math.random() * (640 - width);
				_targetY = Math.random() * (640 - height);
				rotation = Math.atan2(targetY - y, targetX - x) * 180 / Math.PI;
			}
			
			if (_state == ENEMY_GO)
			{
				
				_dirAngle = rotation / 180 * Math.PI;
				
				x = x + _speed * Math.cos(_dirAngle);
				y = y + _speed * Math.sin(_dirAngle);
				
			}
		
		}
		
		public function collision(arr:Array, self:Number):void
		{
			
			for (i = 0; i <= Gameplay.ENEMY_COUNT; i++)
			{
				
				if (i != self)
				{
					
					if (visible)
					{
						
						if (arr[i] != null)
						{
							if (hitTestObject(arr[i]) && arr[i].visible == true)
							{
								
								if (_size >= arr[i].size)
								{
									arr[i].dead();
									arr[i].visible = false;
									size = size + 0.1;
									sounds.PlaySnd("sndEat");
								}
								else
								{
									arr[i].size = arr[i].size + 0.1;
									dead();
									sounds.PlaySnd("sndEat");
								}
								
							}
							
						}
						
					}
					
				}
			}
		
		}
		
		public function dead():void
		{
			
			visible = false;
		}
		
		public function clear():void
		{
		
		}
	
	}

}