package  
{
	
	import flash.display.Sprite;
	import utils.sounds;
	import mochi.as3.*;	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class MainMenu extends MenuDesign
	{
		
		private var _btnPlayClk:Boolean = false;
		private var _btnPlaySound:Boolean = false;
		
		private var _btnHighClk:Boolean = false;
		private var _btnHighSound:Boolean = false;	
		
		private var _backBubbles:Bubbles = new Bubbles();
		
		public function MainMenu() 
		{
			
			btnPlay.alpha = 0.7;
			btnHigh.alpha = 0.7;
			addChild(_backBubbles);
			
		}
		
		public function doo():void {
			
			
			if (btnPlay.hitTestPoint(Main.mouse_x, Main.mouse_y, true)) {
				
				btnPlay.alpha = 1;
				
				if (_btnPlaySound == false) {
					sounds.PlaySnd("sndButton");
					_btnPlaySound = true;
				}				
				
				if (Main.mouseLeftDown) {
					_btnPlayClk = true;
				}
				
			} else {
				_btnPlaySound = false;
				btnPlay.alpha = 0.7;
			}
			
			if (btnHigh.hitTestPoint(Main.mouse_x, Main.mouse_y, true)) {
				
				btnHigh.alpha = 1;
				
				if (_btnHighSound == false) {
					sounds.PlaySnd("sndButton");
					_btnHighSound = true;
				}				
				
				if (Main.mouseLeftDown) {
					_btnHighClk = true;
					//Show mochi highscores
				}
				
			} else {
				_btnHighSound = false;
				btnHigh.alpha = 0.7;
			}			
			
		}
		
		public function get btnPlayClk():Boolean 
		{
			return _btnPlayClk;
		}
		
		public function set btnPlayClk(value:Boolean):void 
		{
			_btnPlayClk = value;
		}
		
		public function clear():void {
			
			if(_backBubbles != null){
				removeChild(_backBubbles);
				_backBubbles.clear();
				_backBubbles = null;
			}
		}		
		
	}

}